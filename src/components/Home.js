import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import { requestPokemonApiAction } from '../redux/actions';
import Pokemon from './Pokemon';

function Home(props) {

  const { requestPokemonApiAction, data, pokemon } = props;
  
  useEffect(()=> {
    requestPokemonApiAction();        
  }, []);
  
  return (
    <div>

      <div>
        {
          data && data.results.map((pk, i) => (<Pokemon key={i} pokemon={pk}></Pokemon>))
        }
      </div>
      
      <div>
        <button 
          disabled={!(data && data.previous)} 
          onClick={() => requestPokemonApiAction(data && data.previous)}>Anterior</button>
        <button 
          disabled={!(data && data.next)} 
          onClick={() => requestPokemonApiAction(data && data.next)}>Siguiente</button>
      </div>

      <div>
        { pokemon && pokemon.name }
      </div>

      <div>
        <img alt="" src={pokemon && pokemon.sprites.front_default} />
      </div>

    </div>
  );
}

const mapStateToProps = state => {
  return ({ data: state.data, pokemon: state.pokemon });
};

const mapDispatchToProps = {
  requestPokemonApiAction
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);