import React from 'react';
import { connect } from 'react-redux';

import { requestPokemonDetailApiAction } from '../redux/actions';

function Pokemon(props) {

  return (
    <div onClick={() => props.requestPokemonDetailApiAction(props.pokemon.url)}>
      { props.pokemon.name }
    </div>
  );
}

const mapDispatchToProps = {
  requestPokemonDetailApiAction
};

export default connect(null, mapDispatchToProps)(Pokemon);