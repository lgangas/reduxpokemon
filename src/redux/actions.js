export const REQUEST_POKEMON_API = 'REQUEST_POKEMON_API';
export const RESPONSE_POKEMON_API = 'RESPONSE_POKEMON_API';

export const REQUEST_POKEMON_DETAIL__API = 'REQUEST_POKEMON_DETAIL__API';
export const RESPONSE_POKEMON_DETAIL_API = 'RESPONSE_POKEMON_DETAIL_API';

export function requestPokemonDetailApiAction(url) {
  return {
    type: REQUEST_POKEMON_DETAIL__API,
    payload: url
  }
}

export function responsePokemonDetailApiAction(pokemon) {
  return {
    type: RESPONSE_POKEMON_DETAIL_API,
    payload: pokemon
  }
}

export function requestPokemonApiAction(url) {
  const _url = url || 'https://pokeapi.co/api/v2/pokemon';
  return {
    type: REQUEST_POKEMON_API,
    payload: _url
  }
}

export function responsePokemonApiAction(data) {
  return {
    type: RESPONSE_POKEMON_API,
    payload: data
  }
}