import { takeLatest, put, all, call } from "redux-saga/effects";

import { fetchPokemon } from "../services/pokemon-service";
import { 
  responsePokemonApiAction, 
  responsePokemonDetailApiAction, 
  REQUEST_POKEMON_API,
  REQUEST_POKEMON_DETAIL__API
} from "./actions";

function * sagaResponsePokemon(action) {
  const data = yield call(fetchPokemon, action.payload);
  yield put(responsePokemonApiAction(data));
}

function * sagaResponsePokemonDetail(action) {
  const data = yield call(fetchPokemon, action.payload);
  yield put(responsePokemonDetailApiAction(data));
}

function * sagaRequestPokemon() {
  yield takeLatest(REQUEST_POKEMON_API, sagaResponsePokemon);
}

function * sagaRequestPokemonDetail() {
  yield takeLatest(REQUEST_POKEMON_DETAIL__API, sagaResponsePokemonDetail);
}

// Take latest toma el ultimo request en cola, los demas los elimina
// Take every ejecuta todos los request

export default function * rootSaga() {
  yield all([
    sagaRequestPokemon(),
    sagaRequestPokemonDetail()
  ])
}