import { RESPONSE_POKEMON_API, RESPONSE_POKEMON_DETAIL_API } from './actions';

function reducer(state = {}, { type, payload }) {
  switch(type) {
    case RESPONSE_POKEMON_API:
      return {
        ...state,
        data: payload
      };
    case RESPONSE_POKEMON_DETAIL_API:
      return {
        ...state,
        pokemon: payload
      };
    default:
      return state;
  }
}

export default reducer